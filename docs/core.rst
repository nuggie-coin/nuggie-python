.. py:currentmodule:: nuggie

nuggie.core Reference
==========================

.. contents::
    :local:

.. automodule:: nuggie.core


Functions
+++++++++++++++

find_tree_root
---------------
.. autofunction:: nuggie.core.find_tree_root

get_difficulty
---------------
.. autofunction:: nuggie.core.get_difficulty

get_reward
---------------
.. autofunction:: nuggie.core.get_reward


Classes
+++++++++++++++

Block
---------------
.. autoclass:: nuggie.core.Block
    :members:
    :undoc-members:

BlockHeader
---------------
.. autoclass:: nuggie.core.BlockHeader

BlocksFile
---------------
.. autoclass:: nuggie.core.BlocksFile
    :members:
    :undoc-members:

Script
---------------
.. autoclass:: nuggie.core.Script
    :members:
    :undoc-members:

ScriptFailure
---------------
.. autoexception:: nuggie.core.ScriptFailure

SingleKeyWallet
---------------
.. autoclass:: nuggie.core.SingleKeyWallet
    :members:
    :undoc-members:

StackItem
---------------
.. autoclass:: nuggie.core.StackItem

StorageManager
---------------
.. autoclass:: nuggie.core.StorageManager
    :members:
    :undoc-members:

Transaction
---------------
.. autoclass:: nuggie.core.Transaction
    :members:
    :undoc-members:
