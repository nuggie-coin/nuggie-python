from collections import namedtuple
import hashlib
import random
import struct
import time
from .transaction import Transaction
from .merkle import find_tree_root
from .script import Script

__all__ = ['get_difficulty', 'get_reward', 'Block', 'BlockHeader']

BlockHeader = namedtuple("BlockHeader", ['version', 'prev_hash', 'merkle_root', 'timestamp',
                                         'difficulty', 'nonce', 'extranonce'])
"""Named Tuple representing a Block Header.

:param version: Version of the block.
:param prev_hash: Hash of the parent block.
:param merkle_root: Merkle root of the block.
:param timestamp: Timestamp of block locking.
:param difficulty: Compact form of the block difficulty.
:param nonce: Nonce value.
:param extranonce: Extra nonce value.
"""


def get_difficulty(val):
    """Expand compact difficulty into full 256-bit number.

    Compact difficultly acts as a type of floating point number.  The first byte is an exponent,
    and the last three are the mantissa.

    :param val: Compact difficulty

    :returns: Full difficulty
    """
    exp = (val & 0xFF000000) >> 24
    mant = val & 0xFFFFFF
    return (mant << exp) & 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF


def get_reward(height):
    """Find the block mining reward at a given height.

    :param height: Height of a block

    :returns: Mining reward for that block
    """
    if height >= 2**26:
        return 0
    exp = (height >> 21) & 31
    return int(50E9/(2**exp))


class Block:
    """Class to handle blocks.

    :param prev_hash: Hash of the parent block.
    :param mine_addr: PAY2PKH address of the mining reward.
    :param difficulty: Compact difficulty for this block.
    :param reward: Block reward for this block.
    :param version: Version of this block.
    :param extranonce: extranonce field.  If None, will be randomly set.

    :ivar locked: Lock state of the block.  Block must be locked to mine.  Once locked,
        only the nonce and extranonce values can be changed.
    :ivar prev_hash: Hash of the parent block.
    :ivar version: Version of this block.
    :ivar time: Timestamp of the block.
    :ivar mine_addr: PAY2PKH address of the mining reward.
    :ivar _difficulty: Compact difficulty of the block.  Should not be directly accessed.
    :ivar difficulty_num: Full difficulty of the block.
    :ivar reward: Block reward.
    :ivar payout: Fees from transactions in the block.
    :ivar nonce: Nonce value.  Should only ever be incremented.
    :ivar extranonce: Additional nonce value.  May be set randomly.
    :ivar transactions: List of transactions.  Transaction 0 is always the coinbase transaction.
    :ivar merkle_root: Root of the merkle tree for this block.
    """

    # pylint: disable=too-many-instance-attributes
    # It's a lot, but justified to allow low-level values to be easily accessed.
    # pylint: disable=too-many-arguments
    # They all need to be set, and using **kwargs makes the code less readable.

    def __init__(self, prev_hash, mine_addr, difficulty, reward=50000000000, version=0,
                 extranonce=None):
        self.locked = False
        self.prev_hash = prev_hash
        self.version = version
        self.time = int(time.time())
        self.mine_addr = mine_addr
        self._difficulty = difficulty
        self.difficulty_num = get_difficulty(difficulty)
        self.reward = reward
        self.payout = 0
        self.nonce = 0
        if extranonce is None:
            self.extranonce = random.getrandbits(32)
        else:
            self.extranonce = extranonce
        paytr = Transaction()
        paytr.add_in(b"I really love nuggets           ", 0)
        paytr.add_out(Script.pay_to_hash_script(self.mine_addr), self.reward)
        paytr.lock()
        paytr.add_preamble(b"I really love nuggets           ", 0, b'\x00')
        self.transactions = [{'data': paytr.to_string(), 'in': 0, 'out': 0, 'hash': paytr.hash}]
        self.merkle_root = find_tree_root([i['hash'] for i in self.transactions])

    def add_transaction(self, trd, totin):
        """Adds transaction to the block.  Updates coinbase transaction.

        :param trd: Bytes of serialized transaction.
        :param totin: Total value of transaction inputs.
        """
        if self.locked:
            raise ValueError("Block locked")
        if len(self.transactions) >= 0xFFFF:
            raise ValueError("Maximum number of transactions exceeded")
        self.time = int(time.time())
        transaction = Transaction.from_string(trd)
        hsh = transaction.hash
        self.transactions.append({'data': trd, 'in': totin, 'out': transaction.outv, 'hash': hsh})
        # update payout
        self.payout = sum([(i['in']-i['out']) for i in self.transactions[1:]])
        # Update coinbase transaction
        paytr = Transaction()
        paytr.add_in(b"I really love nuggets           ", 0)
        paytr.add_out(Script.pay_to_hash_script(self.mine_addr), self.reward+self.payout)
        paytr.lock()
        paytr.add_preamble(b"I really love nuggets           ", 0, b'\x00')
        self.transactions[0] = {'data': paytr.to_string(), 'in': 0, 'out': 0, 'hash': paytr.hash}
        # update merkle root
        self.merkle_root = find_tree_root([i['hash'] for i in self.transactions], hashed=True)

    @property
    def header(self):
        """Serialized header of the block."""
        return struct.pack("!I32s32sQIII", self.version, self.prev_hash, self.merkle_root,
                           self.time, self.difficulty, self.extranonce, self.nonce)

    @property
    def block_hash(self):
        """Block header hash."""
        return hashlib.sha256(hashlib.sha256(self.header).digest()).digest()

    @property
    def difficulty(self):
        """Compact difficulty of the block."""
        return self._difficulty

    @difficulty.setter
    def difficulty(self, val):
        """Set difficulty of the block.  Must be set as the compact version."""
        self._difficulty = val
        self.difficulty_num = get_difficulty(val)

    @property
    def hash_works(self):
        """True if the block hash meets the difficulty requirement."""
        return int.from_bytes(self.block_hash, "big") <= self.difficulty_num

    def mine(self, maxtry=10000):
        """Performs simple POW mining for this block.  Block must be locked.

        :param maxtry: Maximum number of hashes to calculate.

        :returns: Tuple of (If the block was successfully mined, number of hashes attempted
            minus one)
        """
        if self.locked:
            raise ValueError("Block locked")
        i = 0
        while i < maxtry:
            self.nonce += 1
            if self.nonce > 0xFFFFFFFF:
                self.nonce = 0
                self.extranonce = random.getrandbits(32)
            if self.hash_works:
                return True, i
            # Didn't work
            i += 1
        return False, i

    def to_string(self):
        """Returns serialized block as bytes."""
        dat = self.header
        dat += struct.pack("!H", len(self.transactions))
        for transaction in self.transactions:
            dat += struct.pack("!H", len(transaction['data']))
            dat += transaction['data']
        if len(dat) > 2**22:
            raise ValueError("Block raw size too large")
        return dat

    @classmethod
    def from_string(cls, dat):
        """Create a Block from the serialized bytes format.

        :param dat: Bytes of serialized block to load.
        """
        header = dat[0:88]
        dat = dat[88:]
        header = BlockHeader(*struct.unpack("!I32s32sQIII", header))
        numtrs = struct.unpack("!H", dat[0:2])[0]
        dat = dat[2:]
        trs = []
        for _ in range(numtrs):
            trl = struct.unpack("!H", dat[0:2])[0]
            dat = dat[2:]
            trd = dat[0:trl]
            dat = dat[trl:]
            trh = hashlib.sha256(hashlib.sha256(trd).digest()).digest()
            trs.append({'data': trd, 'in': 0, 'out': 0, 'hash': trh})
        block = cls(header.prev_hash, b'\x00'*37, header.difficulty, version=header.version,
                    extranonce=header.extranonce)
        block.merkle_root = header.merkle_root
        block.time = header.timestamp
        block.nonce = header.nonce
        block.transactions = trs
        return block

    def to_file(self, name):
        """Write serialized block to a file.

        :param name: File name to write block to.
        """
        with open(name, 'wb') as block_file:
            block_file.write(self.to_string())

    @classmethod
    def from_file(cls, name):
        """Create a Block from the contents of a file.

        :param name: File name to load serialized block from.
        """
        with open(name) as block_file:
            dat = block_file.read()
        return cls.from_string(dat)
