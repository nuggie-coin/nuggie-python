# -*- coding: utf-8 -*-
"""Module handles wallets.

.. todo::
    * Fix SingleKeyWallet functions for to and from strings.
"""


import hashlib
import ecdsa
from base58 import b58encode_check as b58e
from .transaction import Transaction
from .script import Script

__all__ = ['TYPE_P2PKH_STD', 'SingleKeyWallet']

TYPE_P2PKH_STD = 1


class SingleKeyWallet:
    """Class implements a single key wallet.

    :param sk: ECDSA signing key.
    :parak utxos: List of UTXOs.
    :param transactions: List of transactions.
    :param version: Wallet version.

    :ivar sk: ECDSA signing key.
    :ivar utxo: List of UTXOs available to spend.
    :ivar version: Wallet version.
    :ivar last_ts: Unused.
    :ivar trans: List of transactions.  Unused.
    """
    def __init__(self, sk: ecdsa.SigningKey, utxos, transactions, version=0):
        self.sec_key = sk
        self.utxo = utxos
        self.version = version
        transactions = sorted(transactions, key=lambda i: Transaction.from_string(i).timestamp)
        self.last_ts = 0
        self.trans = transactions

    @classmethod
    def new(cls, seed=None):
        """Create a new empty wallet.

        :param seed: Seed to create SK from.  If none, a key is randomly generated.
        """
        if seed is None:
            secret_key = ecdsa.SigningKey.generate(curve=ecdsa.NIST256p)
        else:
            secexp = ecdsa.util.randrange_from_seed__trytryagain(seed, order=ecdsa.NIST256p.order)
            secret_key = ecdsa.SigningKey.from_secret_exponent(secexp, curve=ecdsa.NIST256p)
        return cls(secret_key, [], [])

    @property
    def address(self):
        """Byte PAY2PKH address of wallet."""
        # Return bytes of address
        mid_hash = hashlib.sha256(self.sec_key.verifying_key.to_string('compressed')).digest()
        key_hash = hashlib.sha256(mid_hash).digest()
        checksum = hashlib.sha256(key_hash).digest()
        addr = key_hash+checksum[-4:]
        return bytes(chr(0), 'ascii') + addr

    @property
    def str_address(self):
        """String (human-readable) PAY2PKH address of wallet."""
        return "NG"+b58e(self.address).decode("ascii")

    def sign(self, message):
        """Sign given message.

        :param message: Data to sign.

        :returns: Signature.
        """
        return self.sec_key.sign(message)

    def get_payment_to_hash(self, recs, fee):
        """Create a transaction paying out to one or more PAY2PKH addresses.
        Remaining funds minus fee returned to self

        :param recs: List of recipients and values.  Form of {'addr': address, 'val': output value}
        :param fee: Fee to pay to miner.

        :returns: Bytes of resulting serialized transaction.
        """
        tval = sum([i['val'] for i in recs])
        if (tval + fee) > sum(i['value'] for i in self.utxo):
            raise ValueError("Wallet does not have sufficient funds")
        transaction = Transaction()
        # Go through UTXOs until we find enough funds
        in_val = 0
        ins = []
        while in_val < (tval + fee):
            ino = self.utxo[0]
            in_val += ino['value']
            ins.append(ino)
            transaction.add_in(ino['hash'], ino['idx'])
            del self.utxo[0]
        for i in recs:
            transaction.add_out(Script.pay_to_hash_script(i['addr']), i['val'])
        transaction.add_out(Script.pay_to_hash_script(self.address), (in_val - tval - fee))
        transaction.lock()
        sig = self.sign(transaction.for_signing())
        for ino in ins:
            transaction.add_preamble(ino['hash'], ino['idx'],
                           Script.pay_to_hash_pre(self.sec_key.verifying_key.to_string('compressed')
                                                  , sig))
        return transaction.to_string()

    def add_utxo(self, txhash, idx, value, tr_type):
        """Add UTXO to wallet.

        Currently supported UTXO types are:
            * TYPE_P2PKH_STD - A transaction output with a standard PAY2PKH script

        :param txhash: Hash of UTXO.
        :param idx: Index of UTXO.
        :param value: Value of UTXO.
        :param tr_type: Type of UTXO.  Currently unused.
        """
        self.utxo.append({'hash': txhash, 'idx': idx, 'value': value, 'type': tr_type})

    # def to_string(self):
    #     hsk = self.sk.to_string().hex()
    #     htr = [i.hex() for i in self.trans]
    #     dat = {'sk': hsk, 'tr': htr, 'utxo': self.utxo, 'ver': self.version,
    #            'utxo_hash': self.utxo_hash.hex(), 'utxo_idx': self.utxo_idx,
    #            'last_ts': self.last_ts}
    #     return json.dumps(dat)

    # @classmethod
    # def from_string(cls, data):
    #     dat = json.loads(data)
    #     sk = ecdsa.SigningKey.from_string(bytes.fromhex(dat['sk']))
    #     tr = [bytes.fromhex(i) for i in dat['tr']]
    #     v = dat['ver']
    #     x = cls(sk, tr, version=v)
    #     return x


# class SeededWallet:
#     def __init__(self, seed, i, transactions, version=0):
#         self.seed = None
#         self.version = version
#         self.i = 0
#         self.keys = []
#         self.addrs = []
#         # generate the first i keys
#         for j in range(i):
#             sk = self.generate_key()
#             self.keys.append(sk)
#             self.addrs.append(bytes(chr(self.version), 'ascii')+
#                               sk.verifying_key.to_string('compressed'))
#
#     def generate_key(self):
#         """Generate next SigningKey from the base seed"""
#         # Get seed for this specific key
#         x = self.seed
#         y = struct.pack("!Q", self.i+1)*8
#         seed = bytes([_a ^ _b for _a, _b in zip(x, y)])
#         secexp = ecdsa.util.randrange_from_seed__trytryagain(seed, order=ecdsa.NIST256p.order)
#         sk = ecdsa.SigningKey.from_secret_exponent(secexp, curve=ecdsa.NIST256p)
#         self.i += 1
#         return sk
