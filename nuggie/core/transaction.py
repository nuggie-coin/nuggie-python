# -*- coding: utf-8 -*-
"""Module handles transactions."""


import struct
import hashlib
import time

# դ

__all__ = ['Transaction']


class Transaction:
    """Class implements Transactions.

    :ivar timestamp: Timestamp of transaction creation.
    :ivar ins: List of transaction inputs.
    :ivar outs: List of transaction outputs.
    :ivar outv: Total value of outputs.
    :ivar inv: Total value of inputs.  If equal to -1, cannot be trusted
    :ivar version: Version of the transaction.
    :ivar locked: True if the transaction is locked.
    """
    # pylint: disable=too-many-instance-attributes
    # It's over, but justified to allow low-level values to be easily accessed.
    def __init__(self):
        self.timestamp = int(time.time())
        self.ins = []
        self.outs = []
        self.outv = 0
        self.inv = 0
        self.version = 0
        self.locked = False

    def add_in(self, txout_hash, txout_idx):
        """Add a new input to the transaction.

        :param txout_hash: Hash of the UTXO being added.
        :param txout_idx: Index of the UTXO being added.
        """
        if self.locked:
            raise ValueError("Transaction Locked")
        if len(self.ins) >= 0xFFFF:
            raise ValueError("Number of inputs exceeded")
        self.ins.append({'hash': txout_hash, 'idx': txout_idx, 'preamble': b""})

    def add_out(self, script, value):
        """Add a new outbut to the transaction.

        :param script: Bytes of the script for the output.
        :param value: Value of the output.
        """
        if self.locked:
            raise ValueError("Transaction Locked")
        if len(self.outs) >= 0xFFFF:
            raise ValueError("Number of outputs exceeded")
        if len(script) >= 0xFFFF:
            raise ValueError("Script size too large")
        self.outs.append({'script': script, 'val': value})
        self.outv += value

    def lock(self):
        """Lock transaction."""
        self.locked = True

    def for_signing(self):
        """Get serialized transaction used for signing.

        :returns: Bytes of the serialized transaction for signing.
        """
        if not self.locked:
            raise ValueError("Transaction not locked")
        istr = b""
        for i in self.ins:
            istr += struct.pack("!32sHH", i['hash'], i['idx'], 0)
        ostr = b""
        for out in self.outs:
            ostr += struct.pack("!QH", out['val'], len(out['script']))
            ostr += out['script']
        tstr = struct.pack(f"!IQH{len(istr)}sH{len(ostr)}s", self.version, self.timestamp,
                           len(self.ins), istr, len(self.outs), ostr)
        return tstr

    def add_preamble(self, txout_hash, txout_idx, preamble, value=None):
        """Add preamble to transaction input.

        :param txout_hash: Hash of the UTXO being spent.
        :param txout_idx: Index of the UTXO being spend.
        :param preamble: Bytes of the preamble to add.
        :param value: Value of the inputs.  Optional, but keeps `inv` valid.
        """
        for i in range(len(self.ins)):
            if self.ins[i]['hash'] == txout_hash and self.ins[i]['idx'] == txout_idx:
                # this is it
                self.ins[i]['preamble'] = preamble
                if value is None:
                    self.inv = -1
                elif self.inv != -1:
                    self.inv += value

    def to_string(self):
        """Get serialized transaction as bytes.

        :returns: Bytes of the serialized transaction.
        """
        if not self.locked:
            raise ValueError("Transaction not locked")
        if self.inv != -1:
            if self.inv < self.outv:
                raise ValueError("Transaction has more out value than in value")
        istr = b""
        for i in self.ins:
            istr += struct.pack("!32sHH", i['hash'], i['idx'], len(i['preamble']))
            istr += i['preamble']
        ostr = b""
        for out in self.outs:
            ostr += struct.pack("!QH", out['val'], len(out['script']))
            ostr += out['script']
        tstr = struct.pack(f"!IQH{len(istr)}sH{len(ostr)}s", self.version, self.timestamp,
                           len(self.ins), istr, len(self.outs), ostr)
        return tstr

    @property
    def hash(self):
        """Transaction hash."""
        return hashlib.sha256(hashlib.sha256(self.to_string()).digest()).digest()

    @classmethod
    def from_string(cls, data):
        """Create a Transaction object from the bytes of a serialized transaction.

        :param data: Bytes of the serialized transaction.
        """
        # pylint: disable=too-many-locals
        # Doesn't (at this time) make sense to split this function up
        version, timestamp, num_ins = struct.unpack("!IQH", data[0:14])
        if version != 0:
            raise ValueError(f"Transaction version {version} not valid")
        data = data[14:]
        ins = []
        for _ in range(num_ins):
            txhash, txidx, plen = struct.unpack("!32sHH", data[0:36])
            data = data[36:]
            preabmle = data[0:plen]
            data = data[plen:]
            ins.append({'hash': txhash, 'idx': txidx, 'preamble': preabmle})
        louts = struct.unpack("!H", data[0:2])[0]
        data = data[2:]
        outs = []
        outv = 0
        for _ in range(louts):
            val, slen = struct.unpack("!QH", data[0:10])
            data = data[10:]
            script = data[0:slen]
            data = data[slen:]
            outs.append({"val": val, "script": script})
            outv += val
        transaction = cls()
        transaction.timestamp = timestamp
        transaction.version = version
        transaction.ins = ins
        transaction.outs = outs
        transaction.outv = outv
        transaction.inv = -1
        transaction.locked = True
        return transaction
