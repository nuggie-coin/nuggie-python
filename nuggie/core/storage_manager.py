# -*- coding: utf-8 -*-
"""Module handles storage and retrieval of blockchain information on disk.

Two classes are provided: an overall StorageManager class, and a class to handle storing blocks on
disk.  Currently, the StorageManager class is responsible for verifying blocks and their
transactions, refusing to store an invalid block.

.. todo::
    * Remove block verification functionality from storage manager - should only store information
    * Hardcode genesis block
    * Cache near-blocks in memory to improve performance
    * Separate storage of block transactions, merkle trees, and block headers for future
      protocol reasons
"""


import gzip
import pathlib
import sqlite3
import struct
from .block import Block, get_reward
from .script import Script, ScriptFailure
from .transaction import Transaction

__all__ = ['FLAG_CURRENT_CHAIN', 'FLAG_GZIP', 'GENESIS_PHASH', 'StorageManager', 'BlocksFile']

FLAG_CURRENT_CHAIN = 0b00000001
FLAG_GZIP = 0b00000010
GENESIS_PHASH = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
                b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'


class StorageManager:
    """Class implements all tasks associated with storing blockchain information on disk.

    :param block_dir: Directory where blocks should be stored.
    :param utxo_db: Path of UTXO sqlite3 database.
    :param nb_db: Path of the near blocks sqlite3 database.
    :param depth: Number of nibbles from the end of block hashes used to determine which
        block file a block should be stored in.

    :ivar block_dir: pathlib Path object where blocks should be stored.
    :ivar utxo_db: pathlib Path object where UTXO database is stored.
    :ivar nb_db: pathlib Path object where near blocks database is stored.
    :ivar utxo_conn: Sqlite3 connection to UTXO database.
    :ivar nb_conn: Sqlite3 connection to near blocks database.
    :ivar nb_base: Lowest height in the near blocks database - considered the oldest near block
    :ivar depth: Number of nibbles from the end of block hashes used to determine which
        block file a block should be stored in.
    :ivar posts: List of block postfixes of depth size.
    :ivar bfiles: List of block files.  Postfix matches the value in posts at the same index.

    .. todo::
        * Evaluate a DataBaseManager and/or BlockFilesManager to reduce attributes.
    """
    # pylint: disable=too-many-instance-attributes
    def __init__(self, block_dir, utxo_db, nb_db, depth=2):
        self.block_dir = pathlib.Path(block_dir)
        self.utxo_db = pathlib.Path(utxo_db)
        self.nb_db = pathlib.Path(nb_db)
        self.utxo_conn = sqlite3.connect(self.utxo_db)
        self.nb_conn = sqlite3.connect(self.nb_db)
        self.nb_base = 0
        self.depth = depth
        self.posts = []
        self.bfiles = []

    def add_block(self, bdat):
        """Add block to blockchain, verifying the block.

        Performs the following steps:
            * Load provided block into a Block object
            * Verify the block is on a non-stale chain
            * Verify that all (non-coinbase) transactions are valid, finding fees
            * Verify the coinbase transaction payout amount is equal to rewards plus fees
            * Write new UTXOs to the UTXO database
            * Remove spent UTXOs from the UTXO database
            * Add block to the near block database
            * Write block to disk

        :param bdat: Serialized block to add.

        .. todo::
            * This function is too big.  Make it smaller
        """
        # pylint: disable=too-many-locals
        blk = Block.from_string(bdat)
        bhash = blk.block_hash
        try:
            height = self.valid_chain(blk.prev_hash)
        except ValueError as error:
            raise ValueError("Failed to put block on current chain") from error
        utxos = []
        ins = []
        fees = 0
        for i, transaction in enumerate(blk.transactions[1:]):
            txheight = (height << 16) + i + 1
            trc = Transaction.from_string(transaction['data'])
            txh = trc.hash
            # Validate the batch of transactions
            try:
                in_val, out_val = self.validate_transaction(trc, txheight)
            except ValueError as error:
                raise ValueError("Transaction failed to validate") from error
            fees += in_val - out_val
            for idx, out in enumerate(trc.outs):
                utxos.append((txh.hex(), idx, out['script'].hex(), out['val'], bhash.hex(), 0,
                              height, txheight))
            for j in trc.ins:
                ins.append((j['hash'].hex(), j['idx']))
        # Check if the coinbase transaction makes sense
        cbc = Transaction.from_string(blk.transactions[0]['data'])
        if cbc.outv != (fees + get_reward(height)):
            raise ValueError("Coinbase transaction value does not match!")
        for idx, out in enumerate(cbc.outs):
            utxos.append((cbc.hash.hex(), idx, out['script'].hex(), out['val'], bhash.hex(),
                          1, height, (height << 16)))
        # Add to the UTXO database
        utxo_cur = self.utxo_conn.cursor()
        utxo_cur.executemany("INSERT INTO transactions (txhash, idx, script, val, block, "
                             "coinbase, height, txheight) "
                             "VALUES (?, ?, ?, ?, ?, ?, ?, ?);", utxos)
        utxo_cur.executemany("DELETE FROM transactions WHERE txhash=? AND idx=?", ins)
        self.utxo_conn.commit()
        utxo_cur.close()
        # Add to the near block DB
        utxo_cur = self.nb_conn.cursor()
        utxo_cur.execute("INSERT INTO blocks (hash, prev_hash, height, head, children) "
                         "VALUES (?, ?, ?, ?, ?);", (bhash.hex(), blk.prev_hash.hex(),
                                                      height, 1, 0))
        utxo_cur.execute("UPDATE blocks SET head=0, children=children+1 WHERE hash=?",
                         (blk.prev_hash.hex(),))
        self.nb_conn.commit()
        utxo_cur.close()
        # Write to disk
        self.write_block_class(blk, height)

    def add_genesis_block(self, bdat):
        """Adds genesis block.  Performs no validation.

        :param bdat: Serialized genesis block.
        """
        blk = Block.from_string(bdat)
        bhash = blk.block_hash
        height = 0
        utxos = []
        for i, transaction in enumerate(blk.transactions):
            trc = Transaction.from_string(transaction['data'])
            txh = trc.hash
            for idx, out in enumerate(trc.outs):
                utxos.append((txh.hex(), idx, out['script'].hex(), out['val'], bhash.hex(),
                              1 if i == 0 else 0, height, (height << 16)+i))
        # Add to the UTXO database
        utxo_cur = self.utxo_conn.cursor()
        utxo_cur.executemany("INSERT INTO transactions (txhash, idx, script, val, block, "
                             "coinbase, height, txheight) "
                             "VALUES (?, ?, ?, ?, ?, ?, ?, ?);", utxos)
        self.utxo_conn.commit()
        utxo_cur.close()
        # Add to the near block DB
        nb_cur = self.nb_conn.cursor()
        nb_cur.execute("INSERT INTO blocks (hash, prev_hash, height, head, children) "
                       "VALUES (?, ?, ?, ?, ?)", (bhash.hex(), GENESIS_PHASH.hex(), height, 1, 0))
        self.nb_conn.commit()
        nb_cur.close()
        # Write to disk
        self.write_block_class(blk, height)

    def validate_transaction(self, transaction, txheight):
        """Validates a transaction, finding total input and output values.

        :param transaction: Transaction represented in a Transaction object.
        :param txheight: Transaction Height of the transaction.

        :returns: Tuple of (Transaction is valid, total transaction input value,
            total transaction output value)

        .. todo::
            * This function is too big.  make it smaller
        """
        # pylint: disable=too-many-locals
        # Check each TX in
        in_val = 0
        out_val = 0
        utxo_cur = self.utxo_conn.cursor()
        nb_cur = self.nb_conn.cursor()
        for i in transaction.ins:
            # Ensure the input is in the UTXO database
            utxo_cur.execute("SELECT val, block, txheight, script FROM transactions "
                             "WHERE txhash=? AND idx=? ORDER BY height",
                             (i['hash'].hex(), i['idx']))
            if utxo_cur.rowcount == 0:
                raise ValueError("Transaction not in UTXO database!")
            val, blk, txh, scr = utxo_cur.fetchone()
            # ensure txheight of the input is lower than ours
            if txh >= txheight:
                raise ValueError("Transaction height lower than input")
            # ensure block is a near block or marked as current chain
            nb_cur.execute("SELECT hash FROM blocks WHERE hash=?", (blk, ))
            if nb_cur.rowcount == 0:
                post = blk[-self.depth:]
                i = self.posts.index(post)
                _, _, flags = self.bfiles[i].get_block(bytes.fromhex(blk))
                if flags & FLAG_CURRENT_CHAIN != FLAG_CURRENT_CHAIN:
                    raise ValueError("Input block not near and not current")
            # ensure script executes properly
            script_vm = Script(i['preamble'], transaction.for_signing())
            try:
                script_vm.execute()
                script_vm.code = bytes.fromhex(scr)
                script_vm.execute()
            except ScriptFailure as error:
                raise ValueError("Script failed to run successfully") from error
            # tx in is good!
            in_val += val
        for out in transaction.outs:
            out_val += out['val']
        if out_val > in_val:
            raise ValueError("Transaction pays out more than inputs")
        return in_val, out_val

    def valid_chain(self, prev_hash):
        """Check if the supplied previous hash is in the near block database and is within
        150 blocks of the highest chain head

        :param prev_hash: Bytes object of previous hash

        :returns: Tuple of (Above conditions are met, height of the block after the given hash,
            string result, string error code)

        .. todo::
             * Remove returning of a string result and error code
        """
        nb_cur = self.nb_conn.cursor()
        nb_cur.execute("SELECT height FROM blocks WHERE hash = ?;", (prev_hash.hex(),))
        # if nb_cur.rowcount <= 0:
        #     nb_cur.close()
        #     raise ValueError("Previous block hash is not close enough to longest chain head")
        last_height = nb_cur.fetchone()[0]
        nb_cur.execute("SELECT max(height) FROM blocks;")
        max_height = nb_cur.fetchone()[0]
        if (max_height - last_height) > 150:
            nb_cur.close()
            raise ValueError("Previous block hash in not close enough to longest chain head")
        self.nb_conn.commit()
        nb_cur.close()
        return last_height + 1

    @property
    def max_height(self):
        nb_cur = self.nb_conn.cursor()
        nb_cur.execute("SELECT max(height) FROM blocks;")
        val = nb_cur.fetchone()[0]
        nb_cur.close()
        return val

    def write_block_class(self, block, height):
        """Write a block to disk, creating new block files if needed.

        :param block: Block object to write to disk.
        :param height: Height of the block being written.
        """
        post = block.block_hash.hex()[-self.depth:]
        if post not in self.posts:
            self.bfiles.append(BlocksFile(post, self.depth, self.block_dir))
            self.posts.append(post)
        i = self.posts.index(post)
        self.bfiles[i].write_block(block, height)

    def get_block_class(self, hsh):
        """Retrieve a block from disk.

        :param hsh: String or Bytes of block header hash to retrieve

        :returns: Tuple of (Block object of requested block, height of requested block)
        """
        if isinstance(hsh, str):
            hsh = bytes.fromhex(hsh)
        post = hsh.hex()[-self.depth:]
        if post not in self.posts:
            return None, -1
        i = self.posts.index(post)
        if not self.bfiles[i].block_present(hsh):
            return None, -1
        blk, height = self.bfiles[i].get_block(hsh)
        return blk, height

    def get_utxo(self, txhash, txidx):
        """Get UTXO from UTXO database

        :param txhash: String or Bytes of requested UTXO transaction hash.
        :param txidx: Transaction index of requested UTXO.

        :returns: Tuple of (UTXO exists, Bytes of UTXO script, UTXO value, Bytes of UTXO
            block hash, UTXO transaction height)
        """
        if isinstance(txhash, bytes):
            txhash = txhash.hex()
        utxo_cur = self.utxo_conn.cursor()
        utxo_cur.execute("SELECT (script, val, block, height) FROM transactions WHERE "
                         "txhash=? AND idx=? ORDER BY height ASC;",
                         (txhash, txidx))
        if utxo_cur.rowcount == 0:
            raise ValueError("UTXO does not exist")
        script, val, blk_hash, height = utxo_cur.fetchone()
        return bytes.fromhex(script), val, bytes.fromhex(blk_hash), height

    def prune_forward(self):
        """Prune near blocks, starting at base and moving forward, removing blocks older
        than a set age.  This process will not progress past a fork."""
        nb_cur = self.nb_conn.cursor()
        utxo_cur = self.utxo_conn.cursor()
        # Get current max head height
        nb_cur.execute("SELECT max(height) FROM blocks WHERE head=1;")
        max_height = nb_cur.fetchone()[0]
        done = False
        while not done:
            # Get lowest block
            nb_cur.execute("SELECT hash, height, children FROM blocks ORDER BY height ASC LIMIT 1;")
            hsh, height, children = nb_cur.fetchone()
            # If we hit a fork, bail
            if children > 1:
                print(f"Block {hsh} is a fork - exiting")
                break
            # If the block is old enough - prune
            if (max_height - height) > 25:
                print(f"Block {hsh} is old enough - validating and pruning")
                # Mark block as on the current chain
                post = hsh[-self.depth:]
                i = self.posts.index(post)
                self.bfiles[i].add_flag(bytes.fromhex(hsh), FLAG_CURRENT_CHAIN)
                # Remove the block from the near block database
                nb_cur.execute("DELETE FROM blocks WHERE hash=?", (hsh,))
            else:
                print(f"Block {hsh} is too new to prune - exiting")
                break
        self.utxo_conn.commit()
        self.nb_conn.commit()
        nb_cur.close()
        utxo_cur.close()

    def prune_backward(self):
        """Prune near blocks backward, removing forks where the head is too far behind the
        longest chain head.  Begins at the newly stale head and removes backwards until the
        fork root is found."""
        nb_cur = self.nb_conn.cursor()
        utxo_cur = self.utxo_conn.cursor()
        # Get current max head height
        nb_cur.execute("SELECT max(height) FROM blocks WHERE head=1;")
        max_height = nb_cur.fetchone()[0]
        # Get a list of current heads
        nb_cur.execute("SELECT hash, height FROM blocks WHERE head=1 ORDER BY height ASC;")
        rows = nb_cur.fetchall()
        for row in rows:
            hsh, height = row
            if (max_height - height) > 25:
                # Time to do the actual pruning
                print(f"Block {hsh} is the start of a fork to be removed")
                nblock = hsh
                while True:
                    nb_cur.execute("SELECT prev_hash, children FROM blocks WHERE hash=?",
                                   (nblock,))
                    pblock, children = nb_cur.fetchone()
                    if children > 1:
                        # end of the fork
                        print(f"Block {nblock} is the end of the fork - exiting")
                        nb_cur.execute("UPDATE blocks SET children=children-1 WHERE hash=?",
                                       (nblock,))
                        break
                    print(f"Block {nblock} is being removed from the fork")
                    # Remove UTXOs
                    utxo_cur.execute("DELETE FROM transactions WHERE block=?", (nblock,))
                    # Remove block
                    nb_cur.execute("DELETE FROM blocks WHERE hash=?", (nblock,))
                    # Move on to the next one
                    nblock = pblock
        self.utxo_conn.commit()
        self.nb_conn.commit()
        nb_cur.close()
        utxo_cur.close()


class BlocksFile:
    """Class to manage a block file on disk.  Stores blocks with a single postfix.

    :param postfix: Hash postfix for this block file
    :param depth: Number of nibbles from the end of block hashes used to determine the postfix.
    :param bpath: Path to block directory.

    Creates two files:
        * A `.blk` file storing the raw serialized blocks.  No structure is stored in this file.
        * A `.idx` file storing index information for the corresponding `.blk` file.

    A flags field is stored for each block.  The flags are:
        * FLAG_CURRENT_CHAIN - Indicates this block is on the current best chain.
        * FLAG_GZIP - Indicates the block is gzip compressed on disk.

    :ivar postfix: Postfix of this block file.
    :ivar depth: Number of nibbles from the end of block hashes used to determine the postfix.
    :ivar bpath: Path to block directory.
    :ivar blocks: List of block hashes in file.
    :ivar bdat: List of block attributes.  Same indices as blocks List.
    """
    def __init__(self, postfix, depth, bpath):
        self.postfix = postfix
        self.depth = depth
        self.bpath = bpath
        self.blocks = []
        self.bdat = []
        self.load()

    def load(self):
        """Load (or create) a block file pair.

        :returns: Number of blocks in the file
        """
        # Check if index file exists
        path = pathlib.Path(self.bpath) / self.postfix
        if not path.with_suffix(".idx").exists():
            path.with_suffix(".idx").touch()
            return 0
        # File exists, load that bad boy
        size = path.with_suffix(".idx").stat().st_size
        if (size % 49) != 0:
            raise ValueError("Index file size incorrect!")
        num_items = size//49
        with open(path.with_suffix(".idx"), 'rb') as idx_file:
            for _ in range(num_items):
                hsh, offset, size, height, flags = struct.unpack("!32sIIQB", idx_file.read(49))
                self.blocks.append(hsh)
                self.bdat.append({'hash': hsh, 'offset': offset, 'size': size, 'flags': flags,
                                  'height': height})
        return num_items

    def block_present(self, hsh):
        """Check if a block is present in the file

        :returns: True if block is present, flase otherwise
        """
        return hsh in self.blocks

    def get_block(self, hsh):
        """Get block from the file

        :param hsh: Bytes object containing the block header hash of the requested block.

        :returns: Tuple of (Block object of requested block, height of the block, block flags)
        """
        idx = self.blocks.index(hsh)
        dat = self.bdat[idx]
        path = pathlib.Path(self.bpath) / self.postfix
        with open(path.with_suffix(".blk"), 'rb') as blk_file:
            blk_file.seek(dat['offset'], 0)
            raw_dat = blk_file.read(dat['size'])
            if dat['flags'] & FLAG_GZIP == FLAG_GZIP:
                return Block.from_string(gzip.decompress(raw_dat)), dat['height'], dat['flags']
            return Block.from_string(raw_dat), dat['height'], dat['flags']

    def write_block(self, block, height, minc=0.75):
        """Write a block to disk.  Note - it is not possible to overwrite an existing block.

        :param block: Block object to write.
        :param height: Height of the block.
        :param minc: Minimum compression ratio required to write the block to disk compressed
        """
        if block.block_hash.hex()[-self.depth:] != self.postfix:
            raise ValueError(f"BFILE {self.postfix} - Postfix does not match")
        hsh = block.block_hash
        if self.block_present(hsh):
            raise ValueError(f"BFILE {self.postfix} - Block already exists in file")
        path = pathlib.Path(self.bpath) / self.postfix
        # first put block into blk file
        offset = path.with_suffix(".idx").stat().st_size
        blk_str = block.to_string()
        cst = gzip.compress(blk_str)
        flags = 0
        if len(blk_str)/len(cst) >= minc:
            blk_str = cst
            flags |= FLAG_GZIP
        size = len(blk_str)
        with open(path.with_suffix(".blk"), 'ab') as blk_file:
            blk_file.write(blk_str)
        with open(path.with_suffix(".idx"), 'ab') as idx_file:
            dat = struct.pack("!32sIIQB", hsh, offset, size, height, flags)
            idx_file.write(dat)
        self.blocks.append(block.block_hash)
        self.bdat.append({'hash': hsh, 'offset': offset, 'size': size, 'flags': flags,
                          'height': height})

    def add_flag(self, hsh, flag):
        """Add a flag (or flags) to a block already stored in this file.

        :param hsh: Bytes object of the hash for the desired block.
        :param flag: Flag to add to the block.
        """
        if hsh.hex()[-self.depth:] != self.postfix:
            raise ValueError(f"BFILE {self.postfix} - Postfix does not match")
        if not self.block_present(hsh):
            raise ValueError(f"BFILE {self.postfix} - Block {hsh.hex()} not present")
        i = self.blocks.index(hsh)
        path = pathlib.Path(self.bpath) / self.postfix
        with open(path.with_suffix(".idx"), 'rb+') as idx_file:
            idx_file.seek((i*49) + 48)
            new_flags = int.from_bytes(idx_file.read(1), 'big') | flag
            self.bdat[i]['flags'] |= flag
            idx_file.seek((i * 49) + 48)
            idx_file.write(struct.pack("!B", new_flags))

    @staticmethod
    def print_flags(flags):
        """Print out a flag bitfield in a human-readable format

        :param flags: Flags to print
        """
        str_flags = []
        if flags & FLAG_CURRENT_CHAIN == FLAG_CURRENT_CHAIN:
            str_flags.append("FLAG_CURRENT_CHAIN")
        if flags & FLAG_GZIP == FLAG_GZIP:
            str_flags.append("FLAG_GZIP")
        print("FLAGS: " + " ".join(str_flags))
