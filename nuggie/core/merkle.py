# -*- coding: utf-8 -*-
"""Module handles tasks associated with merkle trees.

.. todo::
    * Add function to get representation of the full merkle tree.
    * Add function to provide a pruned merkle tree.
    * Add function to validate merkle trees (pruned or full).
"""


import hashlib

__all__ = ['find_tree_root']


def find_tree_root(data, hashed=False):
    """Calculate merkle tree of provided data.

    :param data: List of object in the tree.
    :param hashed: If the provided objects are already hashed.

    :returns: Root hash of the merkle tree.
    """
    level = data
    if not hashed:
        level = [hashlib.sha256(i).digest() for i in level]
    nlevel = []
    while len(level) > 1:
        if len(level) % 2 == 1:
            level.append(level[-1])
        for i in range(len(level)//2):
            nlevel.append(hashlib.sha256(level[i] + level[i+1]).digest())
        level = nlevel.copy()
        nlevel = []
    return level[0]
