# -*- coding: utf-8 -*-
"""Module handles running scripts.

:var ITEM_TYPE_BOOL: Neat
:ivar ITEM_TYPE_ARB: Wowzers!
:data ITEM_TYPE_ARB: Neat~!

.. todo::
    * Implement almost all of the opcodes.
    * Implement an assembler for scripts.
"""


from collections import namedtuple
import hashlib
import ecdsa

__all__ = ['StackItem', 'ITEM_TYPE_BOOL', 'ITEM_TYPE_INT', 'ITEM_TYPE_STR', 'ITEM_TYPE_ARB',
           'Script', 'ScriptFailure']

StackItem = namedtuple("StackItem", ['type', 'data'])
"""Named Tuple for items on the stack.

Items can be of type
  * ITEM_TYPE_BOOL - Single byte representing boolean
  * ITEM_TYPE_INT - Big Endian integer stored in 8 bytes
  * ITEM_TYPE_STR - Non-terminated string
  * ITEM TYPE_ARB - Arbitrary bytes

:param type: Type of the item.  Can be one of:
:param data: Data (in bytes) of the item.
"""


ITEM_TYPE_BOOL = 0
ITEM_TYPE_INT = 1
ITEM_TYPE_STR = 2
ITEM_TYPE_ARB = 3


class Script:
    """Class allows scripts to be run.

    :param code: Bytes of the script to run.
    :param txstr: Bytes of transaction used for verifying signitures.

    :ivar code: Bytes of the script.
    :ivar stack: List of stack_items that make up the stack.
    :ivar altstack: Alternate stack.
    :ivar txstr: Bytes of transaction used for verifying signitures.
    """
    def __init__(self, code, txstr):
        self.code = code
        self.stack = []
        self.altstack = []
        self.txstr = txstr
        self._dctab = self._build_decomp_table()
        self._extab = self._build_exec_table()
        self._bytes = 0

    @staticmethod
    def _build_decomp_table():
        dctab = [(f'OP_UNKNOWN_0x{i:02x}', 0) for i in range(256)]
        dctab[0] = ('OP_FALSE', 0)
        for i in range(75):
            dctab[i+1] = (f'OP_DATA_{i+1}', i+1)
        dctab[118] = ('OP_DUP', 0)
        dctab[135] = ('OP_EQUAL', 0)
        dctab[136] = ('OP_EQUALVERIFY', 0)
        dctab[169] = ('OP_ADDR', 0)
        dctab[172] = ('OP_CHECKSIG', 0)
        return dctab

    def _build_exec_table(self):
        runtab = [self.op_unknown] * 256
        return runtab

    def execute(self):
        """Run script.

        :returns: True if script ran successfully.
        """
        while len(self.code) > 0:
            # read opcode
            opcode = self.code[0]
            self.code = self.code[1:]
            if 1 <= opcode <= 75:
                self._bytes = opcode
            try:
                self._extab[opcode]
            except ScriptFailure as error:
                raise ScriptFailure("Script did not complete successfully") from error
        if len(self.stack) != 0:
            stack_result = self.stack[-1]
            if stack_result.data == b'\x00':
                raise ScriptFailure("Script did not complete successfully")
        raise ScriptFailure("Script did not complete successfully")

    def decompile(self, code):
        """Print decompiled script.

        :param code: Bytes of script to decompile.
        """
        while len(code) > 0:
            # read opcode
            opcode = code[0]
            code = code[1:]
            name, data = self._dctab[opcode]
            print(f"{name}", end=" ")
            if data >= 0:
                raw = code[0:data]
                code = code[data:]
                print(f"{raw.hex()}", end=" ")
        print("")

    def op_unknown(self):
        """Opcode is unknown, fail out."""
        # pylint: disable=no-self-use
        # need to keep function signature identical to opcodes that do use self
        raise ScriptFailure("Unknown opcode!")

    def op_false(self):
        """Put a single false on the stack"""
        self.stack.append(StackItem(ITEM_TYPE_BOOL, b'\x00'))

    def op_data(self):
        """Loads a set number of bytes onto the stack."""
        data = self.code[0:self._bytes]
        self.code = self.code[self._bytes:]
        self.stack.append(StackItem(ITEM_TYPE_ARB, data))

    def op_dup(self):
        """Duplicates the item on the top of the stack."""
        item = self.stack.pop()
        self.stack.append(item)
        self.stack.append(item)

    def op_equal(self):
        """Checks if the two items on the top of the stack are equal, pushing the result
        onto the stack."""
        left = self.stack.pop()
        right = self.stack.pop()
        result = b'\x01' if left.data == right.data else b'\x00'
        self.stack.append(result)

    def op_equalverify(self):
        """Checks if the two items on the top of the stack are equal"""
        left = self.stack.pop()
        right = self.stack.pop()
        if left.data != right.data:
            raise ScriptFailure("OP_EQUALVERIFY found items not equal.")

    def op_addr(self):
        """Generates an address of the version on the top of the stack and the public key
        below."""
        version = self.stack.pop().data
        vk_bytes = self.stack.pop().data
        if len(vk_bytes) != 33:
            # Public (verifying) key is not the correct length!
            raise ScriptFailure("OP_ADDR found verifying key of incorrect length")
        mid_hash = hashlib.sha256(vk_bytes).digest()
        key_hash = hashlib.sha256(mid_hash).digest()
        checksum = hashlib.sha256(key_hash).digest()
        addr = version + key_hash + checksum[-4:]
        self.stack.append(StackItem(ITEM_TYPE_ARB, addr))

    def op_checksig(self):
        """Checks the verifying key at the top of the stack and signature below verify the
        transaction string."""
        try:
            v_key = ecdsa.VerifyingKey.from_string(self.stack.pop().data, curve=ecdsa.NIST256p)
        except ecdsa.keys.MalformedPointError as error:
            raise ScriptFailure("OP_CHECKSIG found an invalid verifying key") from error
        try:
            result = v_key.verify(self.stack.pop().data, self.txstr)
        except (ecdsa.keys.BadSignatureError, ecdsa.util.MalformedSignature) as error:
            raise ScriptFailure("OP_CHECKSIG found an invalid signature") from error
        if not result:
            raise ScriptFailure("OP_CHECKSIG failed to verify the signature")

    @staticmethod
    def pay_to_hash_script(addr):
        """Generate PAY2PKH script for given PAY2PKH address.

        :param addr: PAY2PKH for the script to pay to.

        :returns: Bytes of the script.
        """
        return bytes.fromhex("7600a925")+addr+bytes.fromhex("88ac")

    @staticmethod
    def pay_to_hash_pre(v_key, sig):
        """Generate PAY2PKH preamble.

        :param v_key: Bytes of the Verifying (public) key.
        :param sig: Bytes of the signature.
        """
        return b'\x40' + sig + b'\x21' + v_key


class ScriptFailure(Exception):
    """Exception to signify something went wrong during script execution."""
