# -*- coding: utf-8 -*-
"""Module handles creation and decoding of P2P network messages
"""

import struct
import hashlib
from typing import Tuple, Union, Any


# Constants
# Protocol base information
PROTO_VERSION_INT = 0
PROTO_VERSION = struct.pack("!I", PROTO_VERSION_INT)
MAGIC_P2P_MAINNET = b'\xFD\x9D\x1B\xC4'
NAME_P2P_MAINNET = "mainnet"
UA_REF = b"Nuggie-Ref:0.0.1"
UA_IMPL = b"Nuggie-Python:0.0.1"

# Protocol message types
MESSAGE_ERROR = b'\xFF\xFF'
MESSAGE_CONNECT = b'\x00\x00'
MESSAGE_CONACK = b'\x00\x01'
MESSAGE_PING = b'\x00\x02'
MESSAGE_PONG = b'\x00\x03'

# Feature flags
NODE_DATA = 0b00000000000000000000000000000001
NODE_MINE = 0b00000000000000000000000000000010
NODE_NOBC = 0b10000000000000000000000000000000

ERROR_WRONGNET = 0x00
ERROR_BADNONCE = 0x01


# functions

def get_user_agent(application: bytes, reference=None, implementation=None) -> bytes:
    u_agent = b''
    if reference is None:
        u_agent += UA_REF
    else:
        u_agent += reference
    u_agent += '/'
    if implementation is None:
        u_agent += UA_IMPL
    else:
        u_agent += implementation
    u_agent += '/'
    u_agent += application
    return u_agent


def p2p_message(message_type: bytes, payload: bytes, nonce: bytes):
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[-4:]
    message = p2p_message_header_gen(message_type, nonce, len(payload), check)
    message += payload
    return message


def p2p_validate_check(message: bytes):
    payload = message[22:]
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[-4:]
    return check == payload[18:22]


def p2p_message_header_gen(message_type: bytes, nonce: bytes, size: int,
                           checksum: bytes, network='mainnet') -> bytes:
    """Generate a message header.  The header contains 5 pieces of information:
    * A magic number indicating the network (mainnet, testnet, etc.)
    * A eight byte 'nonce' value used to identify this client to the remote server
    * A two byte message type indicator.  LSB = 0 -> request, LSB = 1 -> reply.  Generally.
    * The size of the payload (excluding header) as a big endian uint64
    * Checksum of the payload as the last four bytes of SHA256(SHA256(payload))
    """
    header = b''
    if network == NAME_P2P_MAINNET:
        header += MAGIC_P2P_MAINNET
    else:
        raise ValueError("Invalid network name")
    if len(message_type) != 2:
        raise ValueError("Message type incorrect size")
    header += message_type
    if len(nonce) != 8:
        raise ValueError("Nonce incorrect size")
    header += nonce
    header += struct.pack("!I", size)
    if len(checksum) != 4:
        raise ValueError("Checksum size incorrect")
    header += checksum
    return header


def p2p_message_header_parse(header: bytes) -> Tuple[str, bytes, bytes, int, bytes]:
    magic = header[0:4]
    if magic == MAGIC_P2P_MAINNET:
        network = NAME_P2P_MAINNET
    else:
        network = 'unknown'
    m_type = header[4:6]
    nonce = header[6:14]
    size = int.from_bytes(header[14:18], "big")
    check = header[18:22]
    return network, m_type, nonce, size, check


def p2p_conn_gen(version: int, services: Union[bytes, int], times: int,
                 remote_nonce: bytes, l_ipaddr: bytes, l_port: int, user_agent: str,
                 height: int) -> bytes:
    """Create a connection initiation request.  Contains the following:
    * Highest protocol version this client can accept
    * Service flags this client is capable of
    * Timestamp this connection request was generated at
    * The connection nonce this client has been assigned by the remote server (if applicable)
    * String of the user agent
    * Height of the highest block this client knows about
    """
    message = b''
    message += struct.pack("!I", version)
    if isinstance(services, int):
        message += struct.pack("!I", services)
    else:
        if len(services) != 4:
            raise ValueError("Services size incorrect!")
        message += services
    message += struct.pack("!Q", times)
    if len(remote_nonce) != 8:
        raise ValueError("Remote nonce size incorrect!")
    message += remote_nonce
    if len(l_ipaddr) != 16:
        raise ValueError("IP Address size incorrect")
    message += l_ipaddr
    message += struct.pack("!H", l_port)
    if len(user_agent) > 255:
        raise ValueError("User agent too long!")
    ua_size = len(user_agent)
    message += struct.pack(f"!B{ua_size}s", ua_size, user_agent.encode("ascii"))
    message += struct.pack("!Q", height)
    return message


def p2p_conn_parse(message: bytes):
    version, services, times, remote_nonce, r_ipaddr, r_port,  ua_len = \
        struct.unpack("!IIQ8s16sHB", message[0:43])
    user_agent = message[43:43+ua_len].decode("ascii")
    height = struct.unpack("!Q", message[43+ua_len:43+ua_len+8])
    return version, services, times, remote_nonce, r_ipaddr, r_port, user_agent, height


def p2p_error_gen(code, string=''):
    return struct.pack("!B", code) + string.encode('ascii')


def p2p_error_parse(message: bytes):
    code = message[0]
    string = message[1:].decode('ascii')
    return code, string


class ChecksumError(Exception):
    pass
