# -*- coding: utf-8 -*-
"""Module handles creating and managing P2P network connections
"""


import ipaddress
import logging
from typing import Union
import random
import time
import zmq
from .Node import NuggieNode
from .p2p_messages import *


__all__ = ['P2PNode']


class P2PNode(NuggieNode):
    def __init__(self, config: Union[str, dict]):
        super().__init__(config)
        # Start network
        port = self.config['network']['p2p-port']
        self.log.info(f"Starting P2P Node on 'tcp://*:{port}'")
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind(f"tcp://*:{port}")
        self.log.info("Socket opened")
        self.connections = []

    def process_messages(self):
        try:
            message = self.socket.recv()
        except zmq.ZMQError:
            return
        self.log.info(f"Received message of {len(message)} bytes")
        header = message[0:22]
        payload = message[22:]
        network, m_type, nonce, size, check = p2p_message_header_parse(header)
        if network != NAME_P2P_MAINNET:
            self.log.error("Message was not for the current network (mainnet)")
            reply = p2p_message(MESSAGE_ERROR, p2p_error_gen(ERROR_WRONGNET, 'invalid_network'),
                                nonce)
            self.socket.send(reply)
            return
        if m_type == MESSAGE_PING:
            self.log.info(f"Received MESSAGE_PING with payload of '{payload.hex()}'")
            reply = p2p_message(MESSAGE_PONG, payload, nonce)
            self.socket.send(reply)
            return
        if m_type == MESSAGE_CONNECT:
            version, services, times, remote_nonce, r_ipaddr, r_port, user_agent, height\
                = p2p_conn_parse(payload)
            if nonce != b'\x00\x00\x00\x00\x00\x00\x00\x00':
                self.log.error(f"Connection request with non-zero nonce '{nonce}'")
                reply = p2p_message(MESSAGE_ERROR, p2p_error_gen(ERROR_BADNONCE, 'nonce-not-zero'),
                                    nonce)
                self.socket.send(reply)
                return
            self.log.info(f"Connection from node at height {height} with UA '{str(user_agent)}'")
            # Register new connection
            n_nonce = struct.pack("!Q", random.getrandbits(64))
            conn = {'addr': r_ipaddr, 'port': r_port, 'nonce': n_nonce, 'services': services}
            self.connections.append(conn)
            ipa = ipaddress.ip_address(self.config['networking']['ip-addr']).packed
            reply = p2p_conn_gen(0, NODE_MINE | NODE_DATA, int(time.time()), n_nonce, ipa,
                                 self.config['networking']['p2p-port'],
                                 str(get_user_agent(self.config['node']['ua-app'])),
                                 self.store.max_height)
            self.socket.send(reply)
            return


class P2PClientReverseTunnel(NuggieNode):
    def __init__(self, config: Union[str, dict], services: int, loglevel=logging.INFO):
        super().__init__(config, loglevel)
        self.context = zmq.Context()
        self.connections = []
        self.services = services | NODE_NOBC

    def open_connection(self, address, port):
        # Open socket
        self.log.info(f"Attempting to open connection to 'tcp://{address}:{port}'")
        sock = self.context.socket(zmq.REQ)
        sock.connect(f"tcp://{address}:{port}")
        # Prepare MESSAGE_CONNECT
        con_payload = p2p_conn_gen(0, self.services, int(time.time()),
                                   b'\x00\x00\x00\x00\x00\x00\x00\x00',
                                   self.config['networking']['ipaddr'],
                                   self.config['networking']['p2p-port'],
                                   str(get_user_agent(self.config['node']['ua-app'])),
                                   self.store.max_height)
        conn = p2p_message(MESSAGE_CONNECT, con_payload, b'\x00\x00\x00\x00\x00\x00\x00\x00')
        self.log.info("Sending connection request...")
        sock.send(conn)
        resp = sock.recv()
        if not p2p_validate_check(resp):
            self.log.error("Received malformed response - Invalid Checksum")
            return False
        header = resp[0:22]
        payload = resp[22:]
        _, m_type, c_nonce, _, _ = p2p_message_header_parse(header)
        if m_type != MESSAGE_CONACK:
            if m_type == MESSAGE_ERROR:
                _, why = p2p_error_parse(payload)
                self.log.error(f"Received MESSAGE_ERROR - '{str(why)}'")
                return False
            return False
        version, services, times, c_nonce2, _, _, user_agent, r_height = p2p_conn_parse(payload)
        if c_nonce != c_nonce2:
            self.log.error("Remote node sent conflicting nonce values")
            return False
        self.log.info(f"Opened connection to remote node 'tcp://{address}:{port}'")
        self.log.info(f"Remote node at height {r_height} with UA '{user_agent}'")
        # TODO: Reverse tunnel setup stuff
        # Add connection to the list
        con_dat = {'addr': address, 'port': port, 'nonce': c_nonce, 'sock': sock,
                   'services': services}
        self.connections.append(con_dat)
        return True

