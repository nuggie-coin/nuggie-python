# -*- coding: utf-8 -*-
"""Module creates the base class for node software
"""


import configparser
import logging
import sys
import time
from typing import Union

from ..core.storage_manager import StorageManager


__all__ = ['NuggieNode']


class NuggieNode:
    def __init__(self, config: Union[str, dict], loglevel=logging.INFO):
        # Load config
        if isinstance(config, dict):
            self.config = config
        else:
            self.config = configparser.ConfigParser()
            self.config.read(config)
        # Setup logging
        self.log = logging.getLogger(__name__)
        self.log.setLevel(loglevel)
        if 'logging' in self.config:
            if 'stdout' in self.config['logging']:
                if self.config['logging']['stdout'].upper() == 'TRUE':
                    std_handle = logging.StreamHandler(sys.stdout)
                    std_handle.setLevel(loglevel)
                    std_handle.setFormatter(logging.Formatter("%(asctime)s %(message)s",
                                                              datefmt='%Y%m%d %I:%M:%S'))
                    self.log.addHandler(std_handle)
            if 'file' in self.config['logging']:
                f_handle = logging.FileHandler(self.config['logging']['file'])
                f_handle.setLevel(loglevel)
                f_handle.setFormatter(logging.Formatter("%(asctime)s %(message)s",
                                                        datefmt='%Y%m%d %I:%M:%S'))
                self.log.addHandler(f_handle)
        # setup Storage
        bdir = self.config['storage']['block-dir']
        utxo_db = self.config['storage']['utxo-db']
        nb_db = self.config['storage']['nb-db']
        depth = self.config['storage']['depth']
        self.store = StorageManager(bdir, utxo_db, nb_db, depth)

    def process_messages(self):
        pass

    def autoloop(self):
        while True:
            self.process_messages()
            time.sleep(float(self.config['node']['loop-delay']))

