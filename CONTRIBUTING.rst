Contributing Guidelines
==========================

The goal of this document is to provide information for anyone who wishes to contribute to the Nuggie Coin project the
information they need to do so successfully and usefully.  This includes both technical information and information
regarding the philosophy used during development.

.. contents:: Table of Contents
    :depth: 2

Documentation and Specifications
---------------------------------

Documentation and Specifications are located `online <https://nuggie-coin.gitlab.io/nuggie-python/>`_.  It is recommended
that contributors be familiar with the specifications regarding any code they wish to contribute.

These documents are automatically generated whenever the `master` branch is updated.

Issue Tracker
----------------

The issue tracker (located `here <https://gitlab.com/nuggie-coin/nuggie-python/-/issues>`_) is a one stop shop for
discussion of bugs, feature requests, and pretty much any other change to this project.

Submitting an Issue
----------------------

General Guidelines
:::::::::::::::::::

* Search for a duplicate issue before submitting.  If your issue is close, but not quite identical, make a comment on
  the close issue.
* Always ensure your issue is with the latest version (preferably the latest development version, but at the very least
  the latest stable release.)
* Only include one issue per report.
* If you find a security or privacy issue, mark the issue as confidential.

Writing a Good Issue Report
::::::::::::::::::::::::::::

Any issue you submit should include the following items.  Issues with missing/deficient information will likely be
rejected.

Title
++++++++++++

The title should include a good, one sentence description of the issue.  `OP_ADDR generates malformed version 0 
PAY2PKH addresses` is a good title.  `Script address bug` is not.

Overview
++++++++++++

The overview is an expanded explanation of what the bug is.  Include:

* An abstracted summary of the incorrect behavior
* A justification of why this is an issue
* Link to the relevant specification if it exists
* If available, the scope of the issue


Steps to Reproduce
+++++++++++++++++++++

Include a detailed list of the steps needed to reproduce the issue.  These may be simple, but in general, including
unneeded information is better than not enough information.

Results
+++++++++++

Include a description of the behaviour the occurs as well as the expected behaviour.  Again, err on the side of including
too much information.

Environment
++++++++++++++

Include information on where the issue was found, including the version used, operating system, etc.  As usual, err on
the side of more information.

Any additional information
++++++++++++++++++++++++++++++
If there's anything else you think is relevant, include it here.

Submitting a Feature Request
-----------------------------

A feature request should follow these guidelines:

* **Provide Details**: Include as specific a description of the proposed feature as possible.  Don't go overboard, but
  give us a good idea of what you want changed and how you would go about it.  Technical details are welcome, but not
  strictly necessary.
* **Justify Your Idea**: Making changes to the project requires work.  You should include a justification of why your 
  proposed change should be made other than 'I think it would be neat.'
* **Represent Your Idea Well**: You don't need to have 100% perfect grammar and formatting, but the easier it is to read
  your request, the easier it will be to have a meaningful discussion about the request.
  
Contributing Code to This Project
-----------------------------------

**NOTE - DURING EARLY DEVELOPMENT, THESE MAY NOT APPLY.**

If you want to submit code, that's great!  Here's how:

Create an Issue Report or Feature Request
::::::::::::::::::::::::::::::::::::::::::

All code submissions should be associated with an entry on the issue tracker.  Follow the above guidelines, and include
a note saying you intend to create a code submission.  Take note of the issue 'number.'

Create a Feature Branch
::::::::::::::::::::::::
Create a new branch of your personal fork of this repository.  The branch should be based on the latest development
branch on the main repository.  The name should start with the issue number and include a *very* brief description of 
the issue.  For example, `192-fix-OP_ADDR` is a good branch name.

Commit Changes
:::::::::::::::::::::::::::::

Making good commits will significantly ease the process of submitting a change.  Here are some guidelines:

* Ensure your name and email address are properly configured.
* Commit messages should include a summary line no longer than 72 characters.  If you need to provide more detail, include a blank line followed by a more detailed summary.
* Each commit should only make one significant change.  This change can be across multiple lines, or even files, but you
  shouldn't, for example, fix two different functions in one commit.  Formatting can always be a single commit.
* Ensure commits do not include extraneous files.  The `.gitignore` should take care of everything, but check before you
  commit.
* Use history-rewriting (i.e. `git rebase -i`) to organize your commits and squash "fixup" commits and revisions.

Create a Merge Request
:::::::::::::::::::::::::

Once your submission is ready (or very close to it), open a merge request against the latest development branch.  Follow
the same guidelines as a Feature Request, substituting details on a proposed feature with details of the changes you made.

A developer will review your changes and make comments.  All discussion topics must be resolved before a change will be
accepted.  It may be necessary to rebase to the `HEAD` of the latest development branch.

Style Guidelines
------------------

Code written should conform to PEP8 as much as is reasonable.  The only specific exception is regarding line length.
For this project, line length limits are 'around 90-ish', but never more than 100.  Where possible, keep lines below 90
characters, but it is better to let a line run slightly long than to make variable names shorter, add confusing line
breaks, or commit some other crime.  To quote PEP8 - "A Foolish Consistency is the Hobgoblin of Little Minds".

In the same spirit, PEP8 is a *guide*, and following it perfectly does not garantee good, readable code. Keep to it as
much as is reasonable, but don't be afraid to deviate where it (in your opinion) makes the code more readable.

Development Philosophy
---------------------------

In no particular order:

* Write code that is 'Pythonic'.  Two great places to learn about what makes code Pythonic are Raymond Hettinger's 
  "Beyond PEP 8 -- Best practices for beautiful intelligible code" talk and PEP20.
* Avoid code repetition as much as is reasonable.  Generally, anything beyond 10 lines should probably be its own function, 
  but use common sense.  It is much harder to fix bugs if the same code exists multiple times across the project.  This
  also extends to higher level functionality.  Two different pieces of code should not do the same job.
* Compartmentalize.  Long functions that perform multiple tasks should be avoided as much as is reasonable.  Making one
  thing do multiple things is how we got the x86 architecture with such beautiful instructions as
  `vfmaddsub123ps xmm0, xmm1, xmmword ptr cs[edi + esi*4+8068860h]` and where there are multiple ways to encode the
  same instruction.
* Be permissive, but give useful errors.  Wherever possible, the code should accept 'invalid' items, but give an error
  back to the user.  Unless asked, allow the user to decide what to do.  The exception is malformed items, which should
  always be rejected.
* JSON is pronounced "JAY-sawn".
* Allow a user to choose how detailed they want to be.  Code should expose the deep, underlying data and logic, but also
  allow the user to just use high-level abstractions.  Properties are great for this.
* Consistency is good, most of the time.  If something is done one way everywhere else, you should probably do it that
  way too.  Unless you shouldn't.
* Code is never self-documenting, but keep trying.  Write good, meaningful comments and docstrings.  If you have any logic
  much more complex than if-else, it probably should be explained.  That said, don't use documentation as an excuse to
  use unintelligible names.  Names are *names*, and they should name what they are.
