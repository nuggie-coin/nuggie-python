import sqlite3

nb = """create table blocks
(
    hash      TEXT,
    prev_hash text,
    height    int,
    head      int,
    children  int
);"""

utxo = """create table transactions
(
    txhash   TEXT,
    idx      int,
    script   text,
    val      int,
    block    TEXT,
    coinbase int,
    height   int,
    txheight int
);"""

c1 = sqlite3.connect("near_blocks.sqlite")
c2 = sqlite3.connect("utxo.sqlite")

cc1 = c1.cursor()
cc2 = c2.cursor()

cc1.execute(nb)
c1.commit()
cc1.close()

cc2.execute(utxo)
c2.commit()
cc2.close()
