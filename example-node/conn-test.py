from nuggie.networking.p2p_messages import *
import zmq
import time

context = zmq.Context()
socket = context.socket(zmq.REQ)
print("Opening connection to remote node at 'tcp://master-node.thenuggie.com:8899'")
socket.connect("tcp://master-node.thenuggie.com:8899")

print("Sending MESSAGE_PING to node")
ping = p2p_message(MESSAGE_PING, b'Hello, nuggie world!', b'00000000')
socket.send(ping)

resp = socket.recv()
header = resp[0:22]
payload = resp[22:]
network, m_type, nonce, size, check = p2p_message_header_parse(header)
if m_type != MESSAGE_PONG:
    print("Did not get back pong?")
else:
    print("Got a PONG back!")
    print(f"-Payload: '{payload.hex()}'")

print("Sending bad network magic to node")
ping = b'\x00' + ping[1:]

socket.send(ping)

resp = socket.recv()
header = resp[0:22]
payload = resp[22:]
network, m_type, nonce, size, check = p2p_message_header_parse(header)
if m_type != MESSAGE_ERROR:
    print("Did not get back an error?")
else:
    print("Got a MESSAGE_ERROR")
    code, debug = p2p_error_parse(payload)
    print(f"-Error code: {code}")
    print(f"-Debug string: '{debug}'")

