import nuggie.core.script
import pathlib
from alive_progress import alive_bar
import time

bpath = pathlib.Path("./blocks")
utxo_db = pathlib.Path("./utxo.sqlite")
nb_db = pathlib.Path("./near_blocks.sqlite")
nblocks = 100
ablocks = 50
bblocks = 76
diff = 0xE8FFFFFF

w = nuggie.core.wallet.SingleKeyWallet.new()
addr = w.address

w2 = nuggie.core.wallet.SingleKeyWallet.new()
addr2 = w2.address

man = nuggie.core.storage_manager.StorageManager(bpath, utxo_db, nb_db)

print(f"Generating {nblocks} fake blocks.")
print(f"Blocks payout to {w.str_address}")

print("Generating base blocks")
lh = b'\x00'*32
bth = b'\x00'
with alive_bar(nblocks) as bar:
    for i in range(nblocks):
        block = nuggie.Block.Block(lh, addr, diff)
        if i == 0:
            block.mine()
            lh = block.block_hash
            man.add_genesis_block(block.to_string())
            bth = nuggie.core.transaction.Transaction.from_string(block.transactions[0]['data']).hash
            w.add_utxo(bth, 0, 50000000000, nuggie.core.wallet.TYPE_P2PKH_STD)
            # sleep to ensure this is a unique TX hash
            time.sleep(1.0)
        elif i == 32:
            tr = w.get_payment_to_hash([{'addr': w2.address, 'val': 40000000000}], 1000000000)
            block.add_transaction(tr, 50000000000)
            block.mine()
            lh = block.block_hash
            man.add_block(block.to_string())
        else:
            block.mine()
            lh = block.block_hash
            man.add_block(block.to_string())
        bar()
# lha = lh
# print("Generating fork 1")
# with alive_bar(ablocks) as bar:
#     for i in range(ablocks):
#         block = nuggie.Block.Block(lha, addr, diff)
#         block.mine()
#         lha = block.block_hash
#         man.add_block(block.to_string())
#         bar()
# lhb = lh
# print("Generating fork 2")
# with alive_bar(bblocks) as bar:
#     for i in range(bblocks):
#         block = nuggie.Block.Block(lhb, addr, diff)
#         block.mine()
#         lhb = block.block_hash
#         man.add_block(block.to_string())
#         bar()
# print("Asking for a prune backward")
# man.prune_backward()
print("Asking for a prune forward")
man.prune_forward()
