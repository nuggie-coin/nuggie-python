\contentsline {section}{\numberline {1}Registers}{4}{}%
\contentsline {subsection}{\numberline {1.1}General}{4}{}%
\contentsline {subsection}{\numberline {1.2}r0}{4}{}%
\contentsline {subsection}{\numberline {1.3}Representation in instructions}{4}{}%
\contentsline {subsection}{\numberline {1.4}Pointer Registers}{4}{}%
\contentsline {section}{\numberline {2}Memory}{5}{}%
\contentsline {subsection}{\numberline {2.1}Word vs. Byte}{5}{}%
\contentsline {subsection}{\numberline {2.2}Data RAM Addressing Modes}{5}{}%
\contentsline {subsubsection}{\numberline {2.2.1}Direct Addressing}{5}{}%
\contentsline {subsubsection}{\numberline {2.2.2}Pointer Addressing}{5}{}%
\contentsline {subsection}{\numberline {2.3}Program ROM Addressing Modes}{5}{}%
\contentsline {subsubsection}{\numberline {2.3.1}Immediate Addressing}{5}{}%
\contentsline {section}{\numberline {3}ALU}{6}{}%
\contentsline {subsection}{\numberline {3.1}Primary ALU}{6}{}%
\contentsline {subsubsection}{\numberline {3.1.1}Command Sequence}{6}{}%
\contentsline {subsubsection}{\numberline {3.1.2}Possible Functions}{6}{}%
\contentsline {section}{\numberline {4}Processor Status Register}{7}{}%
\contentsline {subsection}{\numberline {4.1}Bits 7-5}{7}{}%
\contentsline {subsection}{\numberline {4.2}Bit 4}{7}{}%
\contentsline {subsection}{\numberline {4.3}Bit 3}{7}{}%
\contentsline {subsection}{\numberline {4.4}Bit 2}{7}{}%
\contentsline {subsection}{\numberline {4.5}Bit 1}{7}{}%
\contentsline {subsection}{\numberline {4.6}Bit 0}{7}{}%
\contentsline {section}{\numberline {5}Instruction Execution and Timing}{7}{}%
\contentsline {subsection}{\numberline {5.1}Pipeline}{7}{}%
\contentsline {subsubsection}{\numberline {5.1.1}Instruction Fetch}{8}{}%
\contentsline {subsubsection}{\numberline {5.1.2}Instruction Decode}{8}{}%
\contentsline {subsubsection}{\numberline {5.1.3}Instruction Execution}{8}{}%
\contentsline {section}{\numberline {6}Instruction Reference}{8}{}%
\contentsline {subsection}{\numberline {6.1}Instruction Format}{8}{}%
\contentsline {subsection}{\numberline {6.2}ALU Instructions}{8}{}%
\contentsline {subsubsection}{\numberline {6.2.1}Primary ALU Instruction}{8}{}%
\contentsline {subsubsection}{\numberline {6.2.2}Primary ALU Immediate Instruction}{9}{}%
\contentsline {subsubsection}{\numberline {6.2.3}Bit Shift Left}{9}{}%
\contentsline {subsubsection}{\numberline {6.2.4}Bit Shift Right}{9}{}%
\contentsline {subsubsection}{\numberline {6.2.5}Logical XOR}{9}{}%
\contentsline {subsubsection}{\numberline {6.2.6}Immediate Logical XOR}{10}{}%
\contentsline {subsection}{\numberline {6.3}Data Transfer Instructions}{10}{}%
\contentsline {subsubsection}{\numberline {6.3.1}Register Move}{10}{}%
\contentsline {subsubsection}{\numberline {6.3.2}Immediate Move}{10}{}%
\contentsline {subsubsection}{\numberline {6.3.3}Load From Pointer}{11}{}%
\contentsline {subsubsection}{\numberline {6.3.4}Load To Pointer}{11}{}%
\contentsline {subsubsection}{\numberline {6.3.5}Move In From Pointer}{11}{}%
\contentsline {subsubsection}{\numberline {6.3.6}Move Out To Pointer}{11}{}%
\contentsline {subsection}{\numberline {6.4}Branch Instructions}{12}{}%
\contentsline {subsubsection}{\numberline {6.4.1}Positive Relative Branch}{12}{}%
\contentsline {subsubsection}{\numberline {6.4.2}Negative Relative Branch}{12}{}%
\contentsline {subsubsection}{\numberline {6.4.3}Immediate Branch}{12}{}%
\contentsline {subsubsection}{\numberline {6.4.4}Conditional Branch}{13}{}%
\contentsline {section}{\numberline {7}Conditional Branches}{13}{}%
\contentsline {section}{\numberline {8}IO Space and CSB}{13}{}%
\contentsline {subsection}{\numberline {8.1}IO Space Mapping}{14}{}%
\contentsline {subsection}{\numberline {8.2}CSB}{14}{}%
\contentsline {subsubsection}{\numberline {8.2.1}Connections}{14}{}%
\contentsline {subsubsection}{\numberline {8.2.2}Single Byte Read Timing}{14}{}%
\contentsline {paragraph}{\numberline {8.2.2.1}Timing Description}{15}{}%
\contentsline {subsubsection}{\numberline {8.2.3}Single Byte Write Timing}{15}{}%
\contentsline {paragraph}{\numberline {8.2.3.1}Timing Description}{15}{}%
\contentsline {section}{\numberline {9}Arty A7-35 GPIO Specification}{16}{}%
\contentsline {subsection}{\numberline {9.1}Basic Operation}{16}{}%
\contentsline {subsubsection}{\numberline {9.1.1}Value IO addresses}{16}{}%
\contentsline {subsubsection}{\numberline {9.1.2}Mode IO addresses}{16}{}%
\contentsline {paragraph}{\numberline {9.1.2.1}Output mode}{16}{}%
\contentsline {paragraph}{\numberline {9.1.2.2}Input mode}{16}{}%
\contentsline {subsection}{\numberline {9.2}GPIO Devices}{16}{}%
\contentsline {subsubsection}{\numberline {9.2.1}Device 0}{16}{}%
\contentsline {subsubsection}{\numberline {9.2.2}Device 1}{17}{}%
\contentsline {subsubsection}{\numberline {9.2.3}Device 2}{17}{}%
\contentsline {subsubsection}{\numberline {9.2.4}Device 3}{17}{}%
\contentsline {subsubsection}{\numberline {9.2.5}Device 4}{17}{}%
\contentsline {subsubsection}{\numberline {9.2.6}Device 5}{18}{}%
\contentsline {section}{\numberline {10}Arty A7-35 RGB LED Device Specification}{18}{}%
\contentsline {subsection}{\numberline {10.1}PWM Specification}{18}{}%
\contentsline {subsubsection}{\numberline {10.1.1}PWM Timing}{18}{}%
